import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  const main = document.querySelector('.main');
  const avatar = document.createElement('div');
  avatar.classList.add('image');
  const image = document.createElement('img');
  image.src = 'path/to/image';
  avatar.append(image);
  main.appendChild(avatar);
  const avatarImage = document.querySelector('.image');
  avatarImage.addEventListener('click', function(){
    this.classList.toggle('scaled');
  });
  
});
